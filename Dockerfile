FROM debian:jessie 
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && apt-get install -y \ 
	apache2 \
	git-core \
	php5 \
	php5-curl \
	php5-gd \
	php5-ldap \
	php5-mcrypt \
	php5-mysqlnd \
	php5-readline \
	php5-sqlite \
	php5-tidy \
	php5-xsl \
	postfix \
	sudo

COPY installs/sspak /usr/local/bin/sspak
RUN chmod +x /usr/local/bin/sspak
RUN a2enmod authz_groupfile cgi deflate headers reqtimeout rewrite

EXPOSE 80
VOLUME ["/var/log/apache2"]
CMD ["/bin/bash"]
